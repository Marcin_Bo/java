package recursion;

public class SumTableRecursion {

	public static void main(String[] args) {

		int[] table = {1, 9, 2, 3, 9, 7, 5, 4, 10};
		int tableLength = table.length - 1;
		
		System.out.println(sum(table, tableLength));	
	}
	
	public static int sum(int[] tab, int n){
		
		if(n < 0){
			return 0;
		}
		else if(n == 0) {
			return tab[0];
		}
		else {
			int sum = tab[n] + sum(tab, n - 1);
			return sum;
		}
	}
}
