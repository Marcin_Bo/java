package recursion;

public class PowerRecursion {

	public static void main(String[] args) {
		
		System.out.println(powerRecursively(3, 3));
	}
		
	public static int powerRecursively(int dig, int pow) {
		
		if(pow == 0) {
			return 1;
		}
		else {
			if(pow % 2 == 0) {
				int tmp = powerRecursively(dig, pow/2);
				System.out.println(tmp);
				return tmp * tmp;
			}
			else {
				int tmp = powerRecursively(dig, (pow-1)/2);
				System.out.println(tmp);
				return tmp * tmp * dig;
			}
		}
	}
}
