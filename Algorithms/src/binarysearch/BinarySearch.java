package binarysearch;

public class BinarySearch {

	public static void main(String[] args) {

		int[] table = {989, -5, 1, 98, 12, 23, 199, 987, 45, 654, 833, 5, 123, 777, 56, 0, 87, 454, -72};
		int digit = 56;
		findIndex(table, digit);
	}

	public static void sortTable(int[] table) {

		for(int i = 0; i < table.length; i++) {
			for(int j = 0; j < table.length - 1; j++) {
				if(table[j] > table[j + 1]) {
					int tmp = table[j];
					table[j] = table[j + 1];
					table[j + 1] = tmp;
				}
			}
		}
	}

	public static void findIndex(int[] table, int digit) {

		sortTable(table);
		int firstIndex = 0;
		int lastIndex = table.length - 1;
		int splitTable;
		boolean contain = false;

		while(firstIndex <= lastIndex) {
			splitTable = (firstIndex + lastIndex) / 2;
			if(table[splitTable] == digit) {
				contain = true;
				System.out.println("Find digit: " + table[splitTable] + ", find index: " + firstIndex + ".");
			}
			if(table[splitTable] < digit) {
				firstIndex = splitTable + 1;
			} else {
				lastIndex = splitTable - 1;
			}
		}
		if(contain == false) {
			System.out.println("Not found.");
		}
	}
}
