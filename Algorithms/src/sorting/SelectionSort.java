package sorting;

public class SelectionSort {

	public static void main(String[] args){
		
		int[] table = {91, 5, 2, 33, 7, 55, 4, 1, 0, 8, 9, 3, 2, 1, 6, 15, 74, 0};
		selectionSort(table);
	}
	
	public static void selectionSort(int[] table) {
		
		for(int i = 0; i < table.length; i++) {
			
			for(int j = i; j < table.length; j++) {
				if(table[i] > table[j]) {
					int tmp = table[i];
					table[i] = table[j];
					table[j] = tmp;
				}
			}
		}
		
		for(int i = 0; i < table.length; i++) {
			System.out.println(table[i]);
		}
	}
	
	
}
