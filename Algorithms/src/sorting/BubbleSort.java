package sorting;

public class BubbleSort {

	public static void main(String[] args) {
		
		int[] table = {144, 1, 98, 12, 23, 199, 987, 45, 654, 1000, 5, 123, 777, 56, 87, 0};
		bubbleSorting(table);
	}

	public static void bubbleSorting(int[] table) {
	
		for(int j = 0; j < table.length - 1; j++) {

			for(int i = 0; i < table.length - 1; i++) {
				if(table[i] > table[i + 1]) {
					int tmp = table[i];
					table[i] = table[i + 1];
					table[i + 1] = tmp;
				}
			}
		}

		for (int i = 0; i < table.length; i++) {
			System.out.println(table[i]);
		}
	}
}
