package sorting;

public class InsertionSort {

	public static void main(String[] args){
		
		int[] table = {5, 2, 33, 7, 55, 4, 1, 0, 8, 9, 3, 2, 1, 6, 15, 74};
		insertionSort(table);
	}
	
	public static void insertionSort(int[] table) {
		
		for(int i = 1; i < table.length; i++) {
			
			for(int j = i; j > 0; j--) {
				if(table[j] < table[j - 1]) {
					int tmp = table[j - 1];
					table[j - 1] = table[j];
					table[j] = tmp;
				}
				else {
					break;
				}
			}
		}
		
		for(int i = 0; i < table.length; i++) {
			System.out.println(table[i]);
		}
	}
}
