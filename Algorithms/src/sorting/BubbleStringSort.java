package sorting;

public class BubbleStringSort {

	public static void main(String[] args) {

		String[] tab = {"Mateusz", "Ala", "Olek", "Genowefa", "Robert", "Daniel", "Jadwiga", "Helena", "Izydor", "Helga", "Kamil", "Wierzchosława", "Ida"};

		
		for(int i = 0; i < tab.length; i++) {
			for(int j = 0; j < tab.length - 1; j++) {
				if(tab[j].compareTo(tab[j + 1]) > 0) {
					String tmp = tab[j];
					tab[j] = tab[j + 1];
					tab[j + 1] = tmp;
				}
			}
		}
		
		for(int i = 0; i < tab.length; i++)
			System.out.println(tab[i]);
	}
}
