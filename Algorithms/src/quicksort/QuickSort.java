package quicksort;

public class QuickSort {

	public int partition(int[] table, int p, int q) {
		int i = p;
		for (int j = p; j < q; j++) {
			if (table[j] <= table[q]) {
				int tmp = table[i];
				table[i] = table[j];
				table[j] = tmp;
				i++;
			}
		}

		int tmp = table[q];
		table[q] = table[i];
		table[i] = tmp;

		return i;
	}

	public void sort(int[] table, int p, int q) {
		if (p < q) {
			int m = partition(table, p, q);
			sort(table, p, m - 1);
			sort(table, m + 1, q);
		}
	}

}
