package quicksort;

public class Main {

	public static void main(String[] args) {

		QuickSort qs = new QuickSort();
		int[] table = { 0, 5, 2, 7, 4, 1, 0, 8, 9, 3, 2, 1, 6, -1, 4, -2 };

		qs.sort(table, 0, table.length - 1);

		for (int t : table) {
			System.out.println(t);
		}
	}
}
