package primenumbers;

public class CheckNumber {

	public static void main(String[] args) {

		long n = 982451653L;
		boolean prime = true;
		double square = Math.sqrt(n);
		
		for(long i = 2; i < square; i++) {
			if(n % i == 0) {
				prime = false;
				break;
			}
		}
		if(prime == true) {
			System.out.println("Is prime number");
		}
		else {
			System.out.println("Not prime number");
		}		
	}
}
