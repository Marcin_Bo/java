package primenumbers;

public class EratosthenesSieve {

	public static void main(String[] args) {
		
		int n = 99999;
		int[] tab = new int[n];
		double square = Math.sqrt(n);
		
		for(int i = 0; i < n; i++) {
			tab[i] = i + 2;
		}
		
		for(int i = 0; i < square; i++) {
			if(tab[i] != 0) {
				for(int j = i + tab[i]; j < n; j += tab[i]) {
					tab[j] = 0;
				}
			}
		}
		
		for(int i = 0; i < n; i++) {
			if(tab[i] != 0)
				System.out.println(tab[i]);
		}
	}
}
