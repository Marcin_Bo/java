package sumdigit;

import java.util.Scanner;

public class SumDigit {

	public static void main(String[] args) {

		System.out.println("Give a number: ");
		Scanner scanner = new Scanner(System.in);
		int number = scanner.nextInt();
		System.out.println(sumDigits(number));
		scanner.close();
	}

	public static int sumDigits(int number) {

		int sum = 0;
		while(number > 0) {
			sum = sum + number % 10;
			number = number / 10;
		}
		return sum;
	}
}
