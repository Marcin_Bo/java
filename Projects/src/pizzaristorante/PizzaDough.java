package pizzaristorante;

public interface PizzaDough extends Ingredient {

	public double priceMultiplier();

	public double diameter();
}
