package pizzaristorante;

public class Pizza {

	private PizzaCreator pizzaCreator;
	private PizzaDough pizzaDough;

	public Pizza(PizzaCreator pizzaCreator, PizzaDough pizzaDough) {
		this.pizzaCreator = pizzaCreator;
		this.pizzaDough = pizzaDough;
	}

	public String name() {
		return pizzaCreator.name() + "(" + pizzaDough.diameter() + " cm): " + pizzaCreator.listIngredients();
	}

	public double price() {
		return pizzaDough.price() + pizzaDough.priceMultiplier() * pizzaCreator.price();
	}

	public void updateIgredient(Ingredient ingredient) {
		pizzaCreator.addIngredient(ingredient);
	}
}
