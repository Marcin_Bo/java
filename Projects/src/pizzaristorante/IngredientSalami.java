package pizzaristorante;

public class IngredientSalami implements Ingredient {

	@Override
	public String name() {
		return "salami";
	}

	@Override
	public double price() {
		return 1.2;
	}
}
