package pizzaristorante;

import java.util.ArrayList;

public abstract class PizzaCreator {

	private ArrayList<Ingredient> ingredients;

	public PizzaCreator() {

		ingredients = new ArrayList<Ingredient>();
	}

	public abstract String name();

	public double price() {
		double value = 0;
		for (Ingredient i : ingredients) {
			value += i.price();
		}
		return value;
	}

	public String listIngredients() {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < ingredients.size(); i++) {
			sb.append(ingredients.get(i).name());
			if (i < ingredients.size() - 1) {
				sb.append(", ");
			}
		}
		return sb.toString();
	}

	protected PizzaCreator addIngredient(Ingredient i) {
		ingredients.add(i);
		return this;
	}
}
