package pizzaristorante;

public class IngredientMushrooms implements Ingredient {

	@Override
	public String name() {
		return "mushrooms";
	}

	@Override
	public double price() {
		return 0.7;
	}
}
