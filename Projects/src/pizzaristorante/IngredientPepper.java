package pizzaristorante;

public class IngredientPepper implements Ingredient {

	@Override
	public String name() {
		return "pepper";
	}

	@Override
	public double price() {
		return 0.6;
	}
}
