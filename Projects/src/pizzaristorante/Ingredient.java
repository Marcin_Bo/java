package pizzaristorante;

public interface Ingredient {

	public String name();

	public double price();
}
