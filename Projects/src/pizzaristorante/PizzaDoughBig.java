package pizzaristorante;

public class PizzaDoughBig implements PizzaDough {

	@Override
	public String name() {
		return "big pizza";
	}

	@Override
	public double price() {
		return 21;
	}

	@Override
	public double priceMultiplier() {
		return 1.6;
	}

	@Override
	public double diameter() {
		return 45;
	}
}
