package pizzaristorante;

public class PizzaTypeCapricciosa extends PizzaTypeMargherita {

	@Override
	public String name() {
		return "Capricciosa";
	}

	public PizzaTypeCapricciosa() {
		this.addIngredient(new IngredientHam()).addIngredient(new IngredientMushrooms());
	}
}
