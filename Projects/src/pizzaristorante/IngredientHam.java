package pizzaristorante;

public class IngredientHam implements Ingredient {

	@Override
	public String name() {
		return "ham";
	}

	@Override
	public double price() {
		return 1;
	}
}
