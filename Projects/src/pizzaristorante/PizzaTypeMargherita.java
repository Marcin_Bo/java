package pizzaristorante;

public class PizzaTypeMargherita extends PizzaCreator {

	@Override
	public String name() {
		return "Margherita";
	}

	public PizzaTypeMargherita() {
		this.addIngredient(new IngredientOregano()).addIngredient(new IngredientMozzarella())
				.addIngredient(new IngredientTomatoSauce());
	}
}
