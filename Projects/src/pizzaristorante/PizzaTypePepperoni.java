package pizzaristorante;

public class PizzaTypePepperoni extends PizzaTypeMargherita {

	@Override
	public String name() {
		return "Pepperoni";
	}

	public PizzaTypePepperoni() {
		this.addIngredient(new IngredientPepper()).addIngredient(new IngredientSalami());
	}
}
