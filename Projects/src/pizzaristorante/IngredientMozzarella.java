package pizzaristorante;

public class IngredientMozzarella implements Ingredient {

	@Override
	public String name() {
		return "mozzarella";
	}

	@Override
	public double price() {
		return 0.8;
	}
}
