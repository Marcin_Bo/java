package pizzaristorante;

public class IngredientTomatoSauce implements Ingredient {

	@Override
	public String name() {
		return "tomato sauce";
	}

	@Override
	public double price() {
		return 0.9;
	}
}
