package pizzaristorante;

public class PizzaDoughMedium implements PizzaDough {

	@Override
	public String name() {
		return "medium pizza";
	}

	@Override
	public double price() {
		return 14;
	}

	@Override
	public double priceMultiplier() {
		return 1.3;
	}

	@Override
	public double diameter() {
		return 32;
	}
}
