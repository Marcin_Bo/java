package pizzaristorante;

public class IngredientOregano implements Ingredient {

	@Override
	public String name() {
		return "oregano";
	}

	@Override
	public double price() {
		return 0.4;
	}
}
