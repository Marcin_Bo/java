package carcreator;

public abstract class Car {

	private int carId;
	private String carMake;
	private BodyWork body;
	private Engine engine;
	private StandardType standard;
	private int productionYear;
	private int milesCounter;

	public Car(int carId, String carMake, BodyWork body, Engine engine, StandardType standard, int productionYear,
			int milesCounter) {
		this.carId = carId;
		this.carMake = carMake;
		this.body = body;
		this.engine = engine;
		this.standard = standard;
		this.productionYear = productionYear;
		this.milesCounter = milesCounter;
	}

	protected abstract void createCar();

	protected String carInfo() {
		return "carId = " + carId + ", carMake = " + carMake + ", body = " + body + ", engine = " + engine
				+ ", standard = " + standard + ", productionYear = " + productionYear + ", milesCounter = "
				+ milesCounter;
	}

	public void updateMilesCounter(int miles) {
		milesCounter += miles;
	}

	public int getCarId() {
		return carId;
	}

	public String getCarMake() {
		return carMake;
	}

	public BodyWork getBody() {
		return body;
	}

	public Engine getEngine() {
		return engine;
	}

	public StandardType getStandard() {
		return standard;
	}

	public int getProductionYear() {
		return productionYear;
	}

	public int getMilesCounter() {
		return milesCounter;
	}
}
