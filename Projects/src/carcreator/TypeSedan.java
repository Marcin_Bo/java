package carcreator;

public class TypeSedan extends Car {

	public TypeSedan(int carId, String carMake, BodyWork body, Engine engine, StandardType standard, int productionYear,
			int milesCounter) {
		super(carId, carMake, body, engine, standard, productionYear, milesCounter);
		createCar();
	}

	@Override
	protected void createCar() {
		System.out.println("Create sedan car: " + carInfo());
	}
}
