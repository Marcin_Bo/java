package carcreator;

public class CreateCar {

	public static Car createCar(int carId, String carMake, BodyWork body, Engine engine, StandardType standard,
			int productionYear, int milesCounter) {
		switch (body) {
		case HATCHBACK:
			return new TypeHatchback(carId, carMake, body, engine, standard, productionYear, milesCounter);
		case SEDAN:
			return new TypeSedan(carId, carMake, body, engine, standard, productionYear, milesCounter);
		case WAGON:
			return new TypeWagon(carId, carMake, body, engine, standard, productionYear, milesCounter);
		case SUV:
			return new TypeSuv(carId, carMake, body, engine, standard, productionYear, milesCounter);
		default:
			return null;
		}
	}
}
