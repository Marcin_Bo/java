package carcreator;

public class TypeSuv extends Car {

	public TypeSuv(int carId, String carMake, BodyWork body, Engine engine, StandardType standard, int productionYear,
			int milesCounter) {
		super(carId, carMake, body, engine, standard, productionYear, milesCounter);
		createCar();
	}

	@Override
	protected void createCar() {
		System.out.println("Create suv car: " + carInfo());
	}
}
