package carcreator;

public class Main {

	public static void main(String[] args) {

		CreateCar.createCar(1, "AUDI A1", BodyWork.HATCHBACK, Engine.PETROL, StandardType.SPORT, 2017, 0);
		
		Car toyotaAuris = CreateCar.createCar(2, "Toyota Auris", BodyWork.WAGON, Engine.HYBRID, StandardType.FAMILY, 2016, 16479);
		System.out.println("Car name: " + toyotaAuris.getCarMake() + ", " + "millage: " + toyotaAuris.getMilesCounter());
		
		Car fordFocus = CreateCar.createCar(3, "Ford Focus", BodyWork.SEDAN, Engine.DIESEL, StandardType.LIFE, 2015, 187026);
		fordFocus.updateMilesCounter(45790);
		System.out.println("Car name: " + fordFocus.getCarMake() + ", " + "new millage: " + fordFocus.getMilesCounter());
		
		CreateCar.createCar(4, "Tesla X", BodyWork.SUV, Engine.ELECTRIC, StandardType.BUSINESS, 2017, 0);
	}
}
