package drawingshapes;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Polygon;
import javafx.stage.Stage;

public class Drawing extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Group group = new Group();

		DropShadow ds1 = new DropShadow();
		ds1.setOffsetX(30.0f);
		ds1.setOffsetY(20.0f);
		ds1.setColor(Color.CORAL);

		Circle circle = new Circle();
		circle.setFill(Color.ORANGE);
		circle.setEffect(ds1);
		circle.setCenterX(300.0f);
		circle.setCenterY(135.0f);
		circle.setRadius(100.0f);
		group.getChildren().add(circle);

		Ellipse e1 = new Ellipse(270, 92, 10, 20);
		e1.setStroke(Color.BLACK);
		e1.setFill(Color.WHITE);
		e1.setStrokeWidth(2);
		group.getChildren().add(e1);

		Ellipse e2 = new Ellipse(300, 90, 10, 20);
		e2.setStroke(Color.BLACK);
		e2.setFill(Color.WHITE);
		e2.setStrokeWidth(2);
		group.getChildren().add(e2);

		Polygon polygon = new Polygon();
		polygon.getPoints()
				.addAll(new Double[] { 270.0, 145.0,
										285.0, 120.0,
										300.0, 140.0 });
		group.getChildren().add(polygon);

		Circle circle1 = new Circle();
		circle1.setFill(Color.PURPLE);
		circle1.setCenterX(270.0f);
		circle1.setCenterY(100.0f);
		circle1.setRadius(5.0f);
		group.getChildren().add(circle1);

		Circle circle2 = new Circle();
		circle2.setFill(Color.PURPLE);
		circle2.setCenterX(300.0f);
		circle2.setCenterY(97.0f);
		circle2.setRadius(5.0f);
		group.getChildren().add(circle2);

		Arc arc = new Arc();
		arc.setCenterX(290.0f);
		arc.setCenterY(165.0f);
		arc.setRadiusX(25.0f);
		arc.setRadiusY(30.0f);
		arc.setStartAngle(190.0f);
		arc.setLength(180.0f);
		arc.setType(ArcType.ROUND);
		arc.setFill(Color.RED);
		group.getChildren().add(arc);

		Scene scene = new Scene(group, 500, 500);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
