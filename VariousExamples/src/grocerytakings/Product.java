package grocerytakings;

public class Product {

	private String name;
	private long productId;
	private double price;

	public Product(String name, long productId, double price) {
		this.name = name;
		this.productId = productId;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}
