package grocerytakings;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		List<Product> products = new ArrayList<Product>();

		products.add(new Product("Bread", 1L, 2.99));
		products.add(new Product("Butter", 2L, 6.49));
		products.add(new Product("Gouda cheese", 3L, 4.65));
		products.add(new Product("Apple juice", 4L, 3.95));

		for (Product p : products) {
			System.out.println(p.getName());
		}

		Takings g = new Takings(products);
		System.out.println(String.format("%.2f", g.totalSale()));
	}
}
