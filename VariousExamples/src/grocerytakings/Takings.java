package grocerytakings;

import java.util.List;

public class Takings {

	private double takings;
	private List<Product> products;

	public Takings(List<Product> products) {
		this.products = products;
	}

	public double totalSale() {
		for (Product p : products) {
			takings = takings + p.getPrice();
		}
		return takings;
	}
}
