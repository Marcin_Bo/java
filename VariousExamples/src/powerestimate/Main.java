package powerestimate;

public class Main {

	public static void main(String[] args) {

		DeviceList dl = new DeviceList();
		dl.addDevice(new Device("Fridge", 180, 24));
		dl.addDevice(new Device("TV Set", 32, 3));
		dl.addDevice(new Device("Washing maschine", 2000, 1.15));
		dl.addDevice(new Device("Electric kettle", 1700, 0.05));
		dl.addDevice(new Device("Hotplate", 2000, 2));
		dl.getTotalMonthlyConsumption();
		System.out.println("Your energy costs: " + dl.getTotalMonthyCost() + " $ per month.");
	}
}
