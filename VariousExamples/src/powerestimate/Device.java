package powerestimate;

public class Device {

	private String name;
	private double power;
	private double timeADay;
		
	public Device(String name, double power, double timeADay){
		this.name = name;
		this.power = power;
		this.timeADay = timeADay;
	}
	
	public double getPowerConsumption(){
		return timeADay * power;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPower() {
		return power;
	}

	public void setPower(double power) {
		this.power = power;
	}

	public double getTimeADay() {
		return timeADay;
	}

	public void setTimeADay(double timeADay) {
		this.timeADay = timeADay;
	}
}
