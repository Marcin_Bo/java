package powerestimate;

public class DeviceList {

	private Device[] devices;
	private int nextDevice;
	private double kWhPrice;

	public DeviceList() {
		devices = new Device[20];
		nextDevice = 0;
		kWhPrice = 0.52;
	}

	public void addDevice(Device d) {
		devices[nextDevice] = new Device(d.getName(), d.getPower(), d.getTimeADay());
		nextDevice++;
	}

	public double getTotalMonthlyConsumption() {
		double totalMonthlyConsumption = 0;
		for (int i = 0; i < nextDevice; i++) {
			totalMonthlyConsumption += devices[i].getPowerConsumption() * 30;
		}
		return totalMonthlyConsumption;
	}

	public double getTotalMonthyCost() {
		return getTotalMonthlyConsumption() * kWhPrice / 1000;
	}
}
