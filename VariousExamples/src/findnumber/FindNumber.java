package findnumber;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class FindNumber {

	private int number;
	private int yourGuess;
	private int count = 0;

	Scanner sc = new Scanner(System.in);

	public int drawNumber() {
		number = ThreadLocalRandom.current().nextInt(1, 101);
		return number;
	}

	public int countGuesses() {
		return count++;
	}

	public void guessNumber() {

		yourGuess = sc.nextInt();
		countGuesses();

		if (yourGuess > 100 || yourGuess < 1) {
			System.out.println("You break the rules, number out of range, try again.");
		}
		else if (yourGuess > number) {
			System.out.println("It's lower: ");
			guessNumber();
		} else if (yourGuess < number) {
			System.out.println("It's bigger: ");
			guessNumber();
		} else {
			System.out.println("You guess for " + countGuesses() + " times!");
		}
	}
}
