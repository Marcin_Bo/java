package shippingparceldimension;

public class Main {

	public static void main(String[] args) {
		
		Parcel parcel = new Parcel(24, 60, 20, 30);
		System.out.println("Parcel weight: " + parcel.getWeight());
		System.out.println("Parcel circumference: " + parcel.parcelCircumference());
		System.out.println("Parcel volume: " + parcel.parcelVolume());
		System.out.println("Payment: " + parcel.additionalPayment());
	}

}
