package twodimensionaltable;

public class TwoDimensionalTable {

	public static void main(String[] args) {
		int firstTable[][] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

		int secondTable[][] = { { 9, 8, 7 }, { 6, 5, 4 }, { 3, 2, 1 } };

		int finalTable[][] = new int[3][3];

		showTables(firstTable, secondTable);
		addTables(finalTable, firstTable, secondTable);
		addDiagonalTables(finalTable, firstTable, secondTable);
	}

	public static void showTables(int[][] table1, int[][] table2) {
		for (int i = 0; i < 3; i++) {
			for (int y = 0; y < 3; y++) {
				System.out.print(table1[i][y] + " ");
			}
			System.out.println();
		}
		System.out.println("\t");

		for (int j = 0; j < 3; j++) {
			for (int z = 0; z < 3; z++) {
				System.out.print(table2[j][z] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
	

	public static void addTables(int[][] finalTable, int[][] table1, int[][] table2) {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				finalTable[i][j] = table1[i][j] + table2[i][j];
			}
		}

		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++) {
				System.out.print(finalTable[x][y] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}

	public static void addDiagonalTables(int[][] finalTable, int[][] table1, int[][] table2) {

		int g = 2;
		int h = 2;

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				finalTable[i][j] = table1[i][j] + table2[g][h];
				h--;
			}
			g--;
			h = 2;
		}

		for (int a = 0; a < 3; a++) {
			for (int z = 0; z < 3; z++) {
				System.out.print(finalTable[a][z] + " ");
			}
			System.out.println();
		}
	}
}
