package circle;

public class Circle {

	public static void main(String[] args) {
		double radius = 4.0;

		System.out.println("Circumference when radius " + radius + " is: " + circleCircumference(radius));
		System.out.println("Circle area when radius " + radius + " is: " + circleArea(radius));
	}

	public static double circleCircumference(double radius) {
		return 2 * Math.PI * radius;
	}

	public static double circleArea(double radius) {
		return Math.PI * radius * radius;
	}

}
