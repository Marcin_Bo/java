package minutestohours;

public class MinutesToHours {

	public static void main(String[] args) {

		int minutes = 1046;
		System.out.println(countHours(minutes));
	}

	public static String countHours(int minutes) {

		int countMinutes = 0;
		int countHours = 0;

		for (int i = 0; i <= minutes; i++) {
			countMinutes = countMinutes + 1;
			if (countMinutes == 60) {
				countMinutes = 0;
				countHours += 1;
			}
		}
		return minutes + " minutes is: " + countHours + " hours and " + countMinutes + " minutes.";
	}
}
