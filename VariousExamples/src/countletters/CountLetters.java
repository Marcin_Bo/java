package countletters;

public class CountLetters {

	public static void main(String[] args) {

		String input = "wszedzie dobrze ale w domu najlepiej, prawdziwych przyjaciol poznaje sie w biedzie";
		countLettersInString(input);
	}

	public static int[] countLettersInString(String input) {

		int[] alphabetArray = new int[26];

		for (int i = 0; i < input.length(); i++) {
			char ch = input.charAt(i);
			int value = (int) ch;

			if (value >= 97 && value <= 122) {
				alphabetArray[ch - 'a']++;
			}
		}

		for (int i = 0; i < alphabetArray.length; i++) {
			if (alphabetArray[i] > 0) {
				char ch = (char) (i + 'a');
				System.out.println(ch + " : " + alphabetArray[i]);
			}
		}
		return alphabetArray;
	}
}
