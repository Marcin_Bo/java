package sequenceintable;

public class SequenceInTable {

	public static void main(String[] args) {

		int[] table = { 1, 9, 2, 3, 1, 7, 5, 4, 1, 5, 3, 7, 6, 0, 7 };
		System.out.println(checkSequence(table, 4, 1, 7));
	}

	public static boolean checkSequence(int[] table, int x, int y, int z) {

		boolean sequence = true;

		for (int i = 0; i < table.length - 2; i++) {
			if (table[i] == x & (table[i + 1]) == y & (table[i + 2]) == z) {
				sequence = true;
				break;
			} else {
				sequence = false;
			}
		}
		return sequence;
	}
}
